var api = require('../../utils/api.js');
var charmUrl = api.getCharmUrl();
Page({
  data:{
    motto: '颜值分析',
    img:"",
    info: "",
    age: "",
    beauty: "",
    expression: "",
    glasses: "",
    userInfo: {},
    hasUserInfo: false,
    openId: "",
    nickName: "",
    remark: ""
  },
  uploads: function() {
    var that = this;
    my.chooseImage({
      count:1,
      sourceType: ['camera', 'album'],
      success: (res) => {
        console.info(res + '' + res.apFilePaths[0]);
        that.setData({
          img:res.apFilePaths[0],
          age: "",
          beauty: "",
          expression: "",
          glasses: ""
        });
        my.showLoading({
          content:'玩命分析中...',
          mask:true
        });
        my.uploadFile({
          url:charmUrl, // 开发者服务器地址
          filePath:res.apFilePaths[0], // 要上传文件资源的本地定位符
          fileName: 'file', // 文件名，即对应的 key, 开发者在服务器端通过这个 key 可以获取到文件二进制内容
          fileType: 'image', // 文件类型，image / video / audio
          header: {
            'content-type': 'multipart/form-data'
          },
          formData: {
            'openId':'',
            'nickName':''
          },
          success: (res) => {
            my.hideLoading();
            var data = res.data;
            var faceData = JSON.parse(data);
            console.info(faceData);
            if (faceData.code == 0) {
              that.setData({
                age: faceData.age,
                beauty: faceData.beauty,
                expression: faceData.expression,
                glasses: faceData.glasses
              })
            } else if (faceData.code == 1) {
              my.alert({
                content: '图中不包含人脸哦',
                buttonText: '我知道了',
              });
            } else {
              my.alert({
                content: '未能识别成功',
                buttonText: '我知道了',
              });
            }
          }, 
          fail: function(res) {
            my.hideLoading();
            my.alert({
              content: '小程序远走高飞了',
              buttonText: '我知道了',
            });
          },
        });
      },
    });
  },
  onLoad(query) {
    // 页面加载
    console.info(`Page onLoad with query: ${JSON.stringify(query)}`);
  },
  onReady() {
    // 页面加载完成
  },
  onShow() {
    // 页面显示
  },
  onHide() {
    // 页面隐藏
  },
  onUnload() {
    // 页面被关闭
  },
  onTitleClick() {
    // 标题被点击
  },
  onPullDownRefresh() {
    // 页面被下拉
  },
  onReachBottom() {
    // 页面被拉到底部
  },
  onShareAppMessage() {
    // 返回自定义分享信息
    return {
      title: '花样颜值',
      desc: '有年龄、有颜值',
      path: 'pages/index/index',
    };
  },
});
