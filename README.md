[![Version](https://img.shields.io/badge/version-0.0.1-brightgreen.svg)](https://gitee.com/xshuai/weixinxiaochengxu/)
[![作者](https://img.shields.io/badge/%E4%BD%9C%E8%80%85-%E5%B0%8F%E5%B8%85%E4%B8%B6-7AD6FD.svg)](https://www.xsshome.cn/xai)

# 支付宝小程序-支付宝端源代码
# 支付宝扫一扫 或 搜索 花样颜值Lite
![花样颜值Lite](https://images.gitee.com/uploads/images/2019/0717/173944_a1df0dae_131538.jpeg "花样颜值Lite")
# 使用说明
#### 下载源码 用微信开发工具打开  在 **utils-api.js** 替换自己的域名相关信息即可。
#### 后台代码:[https://gitee.com/xshuai/xai](https://gitee.com/xshuai/xai)
### **大家在使用的时候不要猜测作者的接口授权码等信息进行请求，以防服务器宕机** 

```
       ├── pages                               
       │       └── image                  //图片文件  
       │       └── index                  //颜值分析页面  	   
       ├── utils                                
       │       └── api.js                 //全部的接口url在这里配置  
       ├── app.js                         //全局js配置文件
       ├── app.json                       //全局配置json文件
       ├── app.acss                       //全局wxss文件
       └── snapshot.png                   //上传文件导致支付宝自动加的一个图片 线上用不到本地测试会有
```

### 截图
![小程序首页](https://images.gitee.com/uploads/images/2018/0919/173104_4962ccfa_131538.jpeg "小程序首页")  ![颜值分析](https://images.gitee.com/uploads/images/2018/0919/173454_60cfc195_131538.jpeg "颜值分析")
